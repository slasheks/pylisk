"""
Lisk API Class
"""

#TODO paginate

import lskutils

class lskAPI:
    """

    """

    def __init__(self, url):
        """

        """
        self.url = url

        forall_ = '/api/'

        self._loader = forall_ + 'loader'
        self._accounts = forall_ + 'accounts'
        self._transactions = forall_ + 'transactions'
        self._delegates = forall_ + 'delegates'
        self._version = forall_ + 'peers/version'

    def delegates(self, rtype, delegate_name=None, delegate_address=None):
        """

        """

        if rtype == 'delegates':

            uri = ''

        elif rtype == 'next_forgers':

            uri = '/getNextForgers'

        elif rtype == 'delegate':

            if not delegate_name:
                return {'success':False,
                        'message':'Missing --delegate <delegate name>'}

            uri = '/get?username={}'.format(delegate_name)

        elif rtype == 'forging':

            username = self.delegates('delegate', delegate_name, None)

            uri = '/forging/status?publicKey={}'\
                .format(username['delegate']['publicKey'])

        else:

            return {'success': False,
                    'message': 'Option {} not supported'.format(rtype)}

        return lskutils.http_get(self.url, self._delegates + uri, None)

    def account(self, rtype, delegate_name=None, delegate_address=None):
        """

        """

        if rtype == 'votes':

            if not delegate_address:
                return {'success':False,
                        'message':'Missing --address <delegate address>'}

            uri = '/delegates?address={}'.format(delegate_address)

        else:

            return {'success': False,
                    'message': 'Option {} not supported'.format(rtype)}


        return lskutils.http_get(self.url, self._accounts + uri, None)

    def peers(self, rtype):
        """

        """

        if rtype == 'version':

            response = lskutils.http_get(self.url, self._version, None)

        else:

            return {'success': False,
                    'message': 'Option {} not supported'.format(rtype)}

        return response

    def loader(self, rtype):
        """

        """

        if rtype == 'status':

            uri = '/status'

        elif rtype == 'sync':

            uri = '/status/sync'

        else:

            return {'success': False,
                    'message': 'Option {} not supported'.format(rtype)}

        return lskutils.http_get(self.url, self._loader + uri, None)

    def transactions(self, rtype, transaction=None):
        """

        """

        if rtype == 'transaction':

            if not transaction:
                return {'success':False,
                        'message':'Missing --transaction <tx-id>'}

            uri = '/get?id={}'.format(transaction)

        return lskutils.http_get(self.url, self._transactions + uri, None)
