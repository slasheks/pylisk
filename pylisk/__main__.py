#!/usr/bin/env python3
"""
Can use as cmd line lisky, api calls or db calls

They will both be classes and will take a node address
or the directory where lisk is installed
"""

import re
import sys
import argparse
import json
import lskutils
import yaml
from helpers import lisky
from helpers import lisk_api
from helpers import lisk_db

def main(args, parser):
    """
        Args:
            args: argparse object with all the needed vars
            parser: needed to access the help
    """

    if args.execution_mode == 'lisky':

        lsky = lisky.LSKy(args.install_dir)

        if args.opt == 'get' and args.req == 'delegate':

            response = lsky.get(args.req, args.delegate)

        elif args.opt == 'get' and args.req == 'account' or args.req == 'address':

            response = lsky.get(args.req, args.address)

        elif args.opt == 'get' and args.req == 'transaction':

            response = lsky.get(args.req, args.transaction)

        elif args.opt == 'get' and args.req == 'block':

            response = lsky.get(args.req, args.block)

        elif args.opt == 'list' and args.req == 'delegates':

            response = lsky.list(args.req, args.delegates)

        elif args.opt == 'list' and args.req == 'addresses':

            response = lsky.list(args.req, args.addresses)

        elif args.opt == 'list' and args.req == 'transactions':

            response = lsky.list(args.req, args.transactions)

        elif args.opt == 'list' and args.req == 'blocks':

            response = lsky.list(args.req, args.blocks)

        elif args.opt == 'set' and args.req == 'network':

            # Give it the lisky config location + Key + Value to change
            response = lsky.set(args.req, args.lisky_config_path, args.network)

        else:
            print(usage())
            parser.print_help()
            sys.exit(1)

    elif args.execution_mode == 'liskpy':

        lsk_api = lisk_api.lskAPI(args.url)

        if args.opt == 'get' and args.req == 'version':

            response = lsk_api.peers(args.req)

        elif args.opt == 'get' and args.req == 'delegate':

            response = lsk_api.delegates(args.req, args.delegate)

        elif args.opt == 'get' and args.req == 'delegates':

            response = lsk_api.delegates(args.req)

        elif args.opt == 'get' and args.req == 'forging':

            response = lsk_api.delegates(args.req, args.delegate)

        elif args.opt == 'get' and args.req == 'next_forgers':

            response = lsk_api.delegates(args.req)

        elif args.opt == 'get' and args.req == 'votes':

            response = lsk_api.account(args.req, None, args.address)

        elif args.opt == 'get' and args.req == 'status' or args.req == 'sync':

            response = lsk_api.loader(args.req)

        elif args.opt == 'get' and args.req == 'transaction':

            response = lsk_api.transactions(args.req, args.transaction)

        else:
            print(usage())
            parser.print_help()
            sys.exit(1)

    elif args.execution_mode == 'liskdb':

        lsk_db = lisk_db.lskDB(args.install_dir + 'config.json')

        if args.opt == 'get' and args.req == 'delegates':

            response = lsk_db.delegates(args.req)

        elif args.opt == 'get' and args.req == 'delegate':

            response = lsk_db.delegates(args.req, args.delegate)

        elif args.opt == 'get' and args.req == 'peers':

            response = lsk_db.peers(args.req)

        elif args.opt == 'get' and args.req == 'block':

            response = lsk_db.blocks(args.req, args.block)

        elif args.opt == 'get' and args.req == 'blocks':

            response = lsk_db.blocks(args.req, None, args.blocks)

        elif args.opt == 'get' and args.req == 'transaction':

            response = lsk_db.transactions(args.req, args.transaction)

        elif args.opt == 'get' and args.req == 'transactions':

            response = lsk_db.transactions(args.req, None, args.transactions)

        elif args.opt == 'get' and args.req == 'address':

            response = lsk_db.addresses(args.req, args.address)

        elif args.opt == 'get' and args.req == 'addresses':

            response = lsk_db.addresses(args.req, None, args.addresses)

        elif args.opt == 'get' and args.req == 'upvotes':

            response = lsk_db.custom(args.req, args.public_key)

        elif args.opt == 'get' and args.req == 'downvotes':

            response = lsk_db.custom(args.req, args.public_key)

        elif args.opt == 'get' and args.req == 'fork':

            response = lsk_db.custom(args.req, None, args.block)

        elif args.opt == 'get' and args.req == 'forks':

            response = lsk_db.custom(args.req)

        else:
            print(usage())
            parser.print_help()
            sys.exit(1)

    else:
        print(usage())
        parser.print_help()
        sys.exit(1)

    if args.yaml and args.execution_mode == 'liskpy':
        print(yaml.dump(response))
    else:
        print(json.dumps(response, indent=3))


def usage():
    """
    Usage examples for pylisk

    Args:
        None

    Returns:
        A string with examples on how to use pylisk

    Raises:
        None
    """

    return """Examples:

    python3 pylisk lisky get delegate --delegate slasheks
    python3 pylisk lisky get transaction --transaction 15546460506328965550
    python3 pylisk lisky get block --block 5140353407898744576
    python3 pylisk lisky get address --address 17589464102226817833L
    python3 pylisk lisky set network --network testnet
    python3 pylisk lisky set network --network mainnet

    python3 pylisk lisky list delegates --delegates slasheks gr33ndrag0n
    python3 pylisk lisky list transactions --transactions 15546460506328965550
    python3 pylisk lisky list blocks --blocks 5140353407898744576 15887592492782308955
    python3 pylisk lisky list addresses --addresses 17589464102226817833L 194109334904015388L

    python3 pylisk liskpy get delegate --delegate slasheks
    python3 pylisk liskpy get transaction --transaction 7881560633276667078
    python3 pylisk liskpy get next_forgers
    python3 pylisk liskpy get delegates
    python3 pylisk liskpy get version
    python3 pylisk liskpy get status
    python3 pylisk liskpy get sync
    python3 pylisk liskpy get forging --delegate slasheks

    python3 pylisk liskdb get delegate --delegate slasheks
    python3 pylisk liskdb get delegates
    python3 pylisk liskdb get peers
    python3 pylisk liskdb get block --block 10857411233141233678
    python3 pylisk liskdb get blocks --blocks 10857411233141233678 10857411233141233678
    python3 pylisk liskdb get transaction --transaction 7881560633276667078
    python3 pylisk liskdb get transactions --transactions 7881560633276667078 2484625766645174446
    python3 pylisk liskdb get upvotes --public-key 7beb5f1e8592022fe5272b45eeeda6a1b6923a801af6e1790933cc6a78ed95a1
    python3 pylisk liskdb get downvotes --public-key 7beb5f1e8592022fe5272b45eeeda6a1b6923a801af6e1790933cc6a78ed95a1
    python3 pylisk liskdb get fork --block 13890548296204040564
    python3 pylisk liskdb get forks
    """


if __name__ == '__main__':

    PARSER = argparse.ArgumentParser()

    PARSER.add_argument(dest='execution_mode', action='store',
                        choices=['lisky', 'liskpy', 'liskdb'],
                        help='Execution mode: lisky, API or DB')

    PARSER.add_argument(dest='opt', action='store',
                        choices=['get', 'list', 'set'],
                        help='What action to take (get, list)')

    PARSER.add_argument(dest='req', action='store',
                        choices=['address', 'account', 'delegate', 'delegates',
                                 'version', 'peers', 'votes', 'status', 'sync',
                                 'transaction', 'transactions', 'addresses',
                                 'block', 'blocks', 'network', 'upvotes',
                                 'downvotes', 'forging', 'fork', 'forks'],
                        help='')

    #LISKY side of the house
    PARSER.add_argument('-i','--install-dir',dest='install_dir', action='store',
                        default='/home/liskadmin/lisk-Linux-x86_64/',
                        help='Lisk install directory')

    PARSER.add_argument('-li','--lisky-config',dest='lisky_config_path',
                        action='store',
                        default='/home/liskadmin/.lisky/config.json',
                        help='Lisk install directory')

    PARSER.add_argument('--delegates', dest='delegates', action='store',
                        nargs='+', help='Delegate names')

    PARSER.add_argument('--blocks', dest='blocks', action='store',
                        nargs='+', help='Block IDs')

    PARSER.add_argument('--addresses', dest='addresses', action='store',
                        nargs='+', help='Delegate addresses')

    PARSER.add_argument('--transactions', dest='transactions', action='store',
                        nargs='+', help='Transactions by id')

    #LSK_API side of the house
    PARSER.add_argument('-u','--url',dest='url', action='store',
                        default='http://localhost:7000',
                        help='Lisk enpoint url')

    #LISKDB side of the house

    #All sides, no one is angry here
    PARSER.add_argument('-n','--network', dest='network', action='store',
                        choices=['mainnet', 'testnet'], default='testnet',
                        help='Network name')

    PARSER.add_argument('-d','--delegate', dest='delegate', action='store',
                        help='Delegate name')

    PARSER.add_argument('-b','--block', dest='block', action='store',
                        help='Delegate name')

    PARSER.add_argument('-t','--transaction', dest='transaction', action='store',
                        help='Delegate name')

    PARSER.add_argument('-a', '--address', dest='address', action='store',
                        help='Delegate address')

    PARSER.add_argument('-p', '--public-key', dest='public_key', action='store',
                        help='')

    PARSER.add_argument('--yaml', dest='yaml', action='store_true',
                        help='Delegate address')

    if len(sys.argv[1:]) < 2: print(usage())

    ARGS = PARSER.parse_args()

    main(ARGS, PARSER)
